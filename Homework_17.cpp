﻿ #include <iostream>

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;

public:
    Vector()
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void GetCoordinates()
    {
        std::cout << "X=" << x << ' ' << "Y=" << y << ' ' << "Z=" << z << "\n\n";
    }

    double GetVectorLength()
    {
        double vectorLength = abs(sqrt(x * x + y * y + z * z));
        return vectorLength;
    }

    void PrintVectorLength()
    {
        std::cout << "Vector length is " << GetVectorLength() << "\n\n";
    }
};

int main()
{
    Vector vector(7,-10,8);
    vector.GetCoordinates();
    vector.PrintVectorLength();
} 
